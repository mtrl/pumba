# Pumba

Pumba is a HTML Template library of Phoenix.View and Phoenix.Template components
to be included in a phoenix project.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `pumba` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:pumba, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/pumba](https://hexdocs.pm/pumba).

## Development

IMPORTANT: Before commiting a release with changed assets run:

```bash
make assets
git commit ...
```
## Usage

Create a new Phoenix project and add Pumba as its dependency.

Otherwise you can check the `example/` folder.

Configuration example:

```elixir
config :pumba,
  logo: [
    bold: "MTRL",
    normal: "ab",
    link: "/"
  ],
  menu: [
    main: [
      # child/parent, helper, args, label, icon
      {:child, :page_path, [:index], "Home", "fa fa-home"},
      {:child, :page_path, [:test, :two], "Two", "fa fa-circle-o"},
    ],
    admin: [
      # child/parent, helper, args, label, icon
      {:child, :page_path, [:index], "Home", "fa fa-home"},
      {:child, :page_path, [:test, :two], "Two", "fa fa-circle-o"},
    ]
  ]
```

* `:logo` - definition of string to show in the header and where it should link
* `:menu` - list of menu items to be put in the sidebar

### Static Assets

You can use this package to serve its static assets (css, js, etc.) by using it
in your endpoint:

```elixir
defmodule HelloWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :hello
  
  ...
  use Pumba.Static
  ...
  
end
```

The static assets will be server from `/pumba`.

### Views

In order to get function definitions in your layout view, you need to use `Pumba`
there:

```elixir
# layout_view.ex
defmodule ExampleWeb.LayoutView do
  use ExampleWeb, :view
  use Pumba
end
```

In your `app.html.eex` layout, include the stylesheets from `Pumba`. Additionaly
include `upper` and `lower` parts of the template that were injected by `Pumba`
in your `layout_view.ex`. You can checkout the example in
`example/lib/example_web/templates/layout/app.html.eex`

```html
<!-- app.html.eex -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Hello Example!</title>
        <%= PumbaView.stylesheets() %>
        <link rel="stylesheet" href="<%= static_path(@conn, "/css/app.css") %>">
    </head>

    <body>
        <%= upper(@conn) %>
        <%= render @view_module, @view_template, assigns %>
        <%= lower(@conn) %>

        <script src="<%= static_path(@conn, "/js/app.js") %>"></script>
    </body>
</html>
```
