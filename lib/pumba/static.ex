defmodule Pumba.Static do
  @moduledoc """
  Static Module can be used within other Phoenix projects in order
  to include serving the static assets, have access to base url, and
  mounted path "/pumba".

  ## Example

      use Pumba.Static
  """

  import Pumba.Utils, only: [baseurl: 0]

  defmacro __using__(_) do
    quote do
      plug(
        Plug.Static,
        at: baseurl(),
        from: :pumba,
        gzip: false,
        only: ~w(css fonts images js favicon.ico robots.txt)
      )
    end
  end
end
