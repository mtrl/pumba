defmodule Pumba.Components.MenuItem do
  @moduledoc "Struct that holds the information for the menu item"

  @active_class "active"

  alias Pumba.Components

  @type t :: %Components.MenuItem{
          href: binary,
          label: binary,
          icon: binary,
          active: binary
        }

  defstruct href: "#", label: "Home", icon: "fa-home", active: @active_class

  def active?(path, href) do
    case path == href do
      true -> @active_class
      _ -> ""
    end
  end
end
