defmodule Pumba.Components.Logo do
  @moduledoc "Struct that holds the text and link for the logo"

  alias Pumba.Components

  @type t :: %Components.Logo{bold: binary, normal: binary, link: binary, icon: binary}

  defstruct [:bold, :normal, :link, :icon]
end
