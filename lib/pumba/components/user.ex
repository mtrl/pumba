defmodule Pumba.Components.User do
  @moduledoc "Struct that holds the user information"

  @type t :: %Pumba.Components.User{
          fullname: binary,
          username: binary,
          image_url: binary,
          profile_link: binary,
          sign_out_link: binary
        }

  defstruct [:fullname, :username, :image_url, :profile_link, :sign_out_link]
end
