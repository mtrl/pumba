defmodule Pumba.Utils do
  def baseurl() do
    Application.get_env(:pumba, :path, "/pumba")
  end
end
