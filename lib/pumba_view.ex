defmodule PumbaView do
  @moduledoc """
  HTML view functions that can be used in phoenix projects to render
  Pumba components.
  """

  use Phoenix.View,
    root: "lib/templates",
    path: ""

  use Phoenix.HTML

  import Pumba.Utils, only: [baseurl: 0]

  @stylesheets ~w[font-awesome.min.css select2.min.css app.css]
  @scripts ~w[app.js select2.min.js]

  def stylesheets() do
    @stylesheets
    |> Enum.map(&Enum.join([baseurl(), "css", &1], "/"))
    |> render_many(__MODULE__, "stylesheet.html", as: :url)
  end

  def scripts() do
    @scripts
    |> Enum.map(&Enum.join([baseurl(), "js", &1], "/"))
    |> render_many(__MODULE__, "script.html", as: :url)
  end

  def upper(opts) do
    render(__MODULE__, "upper.html", opts)
  end

  def lower() do
    render(__MODULE__, "lower.html")
  end

  def menu(title, items) do
    render(__MODULE__, "menu.html", title: title, items: items)
  end
end
