defmodule Pumba do
  @moduledoc """
  Pumba defines the base behaviour to be implemented by layout_view.ex
  of phoenix projects that want to use `PumbaView` templates.
  """

  alias __MODULE__

  defmacro __using__(_) do
    quote do
      def upper(conn) do
        PumbaView.upper(
          logo: logo(),
          user: user(conn),
          menu: menu_list(conn),
          state: state(conn),
        )
      end

      def lower(_conn) do
        PumbaView.lower()
      end

      def logo do
        with logo <- Application.get_env(:pumba, :logo, []),
             bold <- Keyword.get(logo, :bold, ""),
             normal <- Keyword.get(logo, :normal, ""),
             icon <- Keyword.get(logo, :icon, ""),
             link <- Keyword.get(logo, :link, "/") do
          %Pumba.Components.Logo{bold: bold, normal: normal, link: link, icon: icon}
        end
      end

      def user(_conn) do
        %Pumba.Components.User{
          fullname: "Mario Rossi",
          username: "mariorossi77",
          image_url: "http://www.onefamilygroup.eu/wp-content/uploads/2013/11/team-4.jpg",
          profile_link: "/",
          sign_out_link: "/"
        }
      end

      def menu_list(conn) do
        conn
        |> menu()
        |> Enum.map(fn {title, items} ->
          items =
            items
            |> Enum.map(&menu_item(conn, &1))

          {title, items}
        end)
      end

      def menu(conn) do
        Application.get_env(:pumba, :menu)
      end

      def menu_item(conn, elem = {term, helper, args, label, icon}) do
        with href <- route(helper, [conn] ++ args),
             active <- Pumba.Components.MenuItem.active?(conn.request_path, href) do
          %Pumba.Components.MenuItem{href: href, label: label, icon: icon, active: active}
        end
      end

      def state(conn) do
        [
          %{label: "State one", href: "/"},
          %{label: "State two", href: "/"},
          %{label: "State three", href: "/"}
        ]
      end

      def route(helper, args) do
        router_helpers() |> Kernel.apply(helper, args)
      end

      @doc """
      Retrieve the module name of `ExampleWeb.Router.Helpers` so that we can
      call them for menu items.
      """
      def router_helpers do
        __MODULE__
        |> Kernel.to_string()
        |> String.split(".")
        |> Enum.take(2)
        |> Kernel.++(["Router", "Helpers"])
        |> Enum.join(".")
        |> String.to_atom()
      end

      defoverridable logo: 0, user: 1, menu: 1, state: 1
    end
  end
end
