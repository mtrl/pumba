exports.config = {
  // See http://brunch.io/#documentation for docs.
  files: {
    javascripts: {
      // To use a separate vendor.js bundle, specify two files path
      // http://brunch.io/docs/config#-files-
      // joinTo: {
      //   "js/app.js": /^js/,
      //   "js/vendor.js": /^(?!js)/
      // }
      joinTo: "js/app.js"
    },
    stylesheets: {
        joinTo: "css/app.css",
        order: {
            after: "css/app.scss"
        }
    },
    templates: {
      joinTo: "js/app.js"
    }
  },

  conventions: {
    // This option sets where we should place non-css and non-js assets in.
    // By default, we set this to "/assets/static". Files in this directory
    // will be copied to `paths.public`, which is "priv/static" by default.
    assets: /^(static)/
  },

  // Phoenix paths configuration
  paths: {
    // Dependencies and current project directories to watch
    watched: ["static", "css", "js", "vendor"],
    // Where to compile files to
    public: "../priv/static"
  },

  // Configure your plugins
  plugins: {
    babel: {
    // Do not use ES6 compiler in vendor code
    ignore: [/vendor/]
    },
    copycat: {
        "css": [
            "node_modules/font-awesome/css/font-awesome.min.css",
            "node_modules/select2/dist/css/select2.min.css"
        ], // copy these files into priv/static/css/
        "js": [
            "node_modules/select2/dist/js/select2.min.js"
        ],
        "fonts": [
            "node_modules/font-awesome/fonts/fontawesome-webfont.ttf",
            "node_modules/font-awesome/fonts/fontawesome-webfont.woff",
            "node_modules/font-awesome/fonts/fontawesome-webfont.woff2"
        ],
        verbose: true
    },
    sass: {
        mode: "native",
        options: {
            includePaths: ['node_modules/bootstrap/scss'],
            precision: 8
        }
    }
  },

  modules: {
    autoRequire: {
      "js/app.js": ["js/app"]
    }
  },

  npm: {
      enabled: true,
      globals: {
          $: 'jquery',
          jQuery: 'jquery',
          Popper: 'popper.js',
          bootstrap: 'bootstrap'
      }
  }
};
