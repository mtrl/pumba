defmodule ExampleWeb.PageController do
  use ExampleWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

  def test(conn, %{"value" => value}) do
    render conn, "test.html", value: value
  end
end
