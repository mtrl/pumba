.PHONY: all assets

all: assets

assets:
	cd assets; brunch build -p
