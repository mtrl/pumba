defmodule Pumba.MixProject do
  use Mix.Project

  def project do
    [
      app: :pumba,
      version: "0.1.0",
      elixir: "~> 1.6",
      compilers: [:phoenix] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      # Docs
      name: "Pumba",
      source_url: "https://gitlab.com/mtrl/pumba",
      homepage_url: "http://hexdocs.pm/pumba",
      docs: [main: "readme", extras: ["README.md"]]
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:phoenix, "~> 1.3.0"},
      {:phoenix_html, "~> 2.10"},
      {:ex_doc, "~> 0.16", only: :dev, runtime: false}
    ]
  end
end
